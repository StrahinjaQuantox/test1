<?php
    session_start();
    include __DIR__ . "/vendor/autoload.php";
    use test\controllers\UserController;
    use test\controllers\IndexController;
    use test\controllers\AuthController;
   $dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
   $dotenv->load();

    /*use test\database\connection;
    $controller = new \test\controllers\index();
    print_r($controller->index());*/

//TODO implement proper router
    $router = new Bramus\Router\Router();
    
    
    $router->set404(function () {
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
        echo '404, route not found!';
    });
    // router for index page
    $router->get('/', function(){
      $controller = new IndexController();
      echo $controller->index();
      
    });
    //Routers for user authentification pages
    $router->match('GET|POST','/login', function(){
        $controllers = new AuthController();
        echo $controllers->login();
    });
    
    $router->get('/logout', function(){
        $controller = new AuthController();
        echo $controller->logout();
    });
    
    $router->match('GET|POST','/registration', function(){
        $controller = new AuthController();
        echo $controller->registration();
    });
    
    //Routers for user management functions
    $router->get('/delete', function(){
        $controller = new UserController();
        echo $controller->delete();
    });
    
    $router->match('GET|POST','/create', function(){
        $controller = new UserController();
        echo $controller->create();
    });
    
    $router->match('GET|POST', '/update/{user}', function ($user){
        $controller = new UserController();
        echo $controller->update($user);
    });
    
    $router->get('/view/{user}', function($user){
        $controller = new UserController();
        echo $controller->view($user);
    });
   
    $router->run();

