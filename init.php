<?php
include __DIR__ . '/vendor/autoload.php';
$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$connection = new ByJG\Util\Uri('mysql://'. $_ENV['DB_USER'] . ":"
        . $_ENV["DB_PASS"] . "@" . $_ENV["DB_HOST"] 
        . ":3306/" . $_ENV['DB_NAME']);

$migration = new ByJG\DbMigration\Migration($connection, 'src/migrations');

//register a database
$migration->registerDatabase('mysql', \ByJG\DbMigration\Database\MySqlDatabase::class);

$status = $migration->getCurrentVersion();

if($status['version'] > 0 && $status['status'] === "complete"){
    die('database is already completed ');
}



// Add a callback progress function to receive info from the execution
$migration->addCallbackProgress(function ($action, $currentVersion, $fileInfo) {
    echo "$action, $currentVersion, ${fileInfo['description']}\n";
});

//creates database if not exists
$migration->prepareEnvironment();

$migration->createVersion();

// Restore the database using the "base.sql" script
// and run ALL existing scripts for up the database version to the latest version
$migration->reset();



/*$password = password_hash("admin", PASSWORD_BCRYPT);
\test\database\connection::getConnection()
    ->query("insert into users (username, email, users.pass, role_id) 
values ('".$_ENV["SITE_ADMIN"]."', '".$_ENV["SITE_ADMIN_EMAIL"]. "', '". password_hash($_ENV["SITE_ADMIN_PASS"], PASSWORD_BCRYPT)."', 1)");
 * */

$seeder = new tebazil\dbseeder\Seeder(test\database\connection::getConnection());
$generator = $seeder->getGeneratorConfigurator()->getFakerConfigurator();

$seeder->table('roles')->columns([
    'id',
    'name' => 'admin'
]);
$seeder->table('roles')->columns([
    'id',
    'name' => 'restricted'
]);
$seeder->table('permissions')->columns([
    'id',
    'name' => 'user_management'
]);
$seeder->table('permissions')->columns([
    'id',
    'name' => 'view_users'
]);
$seeder->table('roles')->columns([
    'id',
    'name' => 'logout'
]);
$seeder->table('users')->columns([
    'id',
    'username' => $_ENV['SITE_ADMIN'],
    'pass' => password_hash($_ENV['SITE_ADMIN_PASS'], PASSWORD_BCRYPT),
    'email' => $_ENV['SITE_ADMIN_EMAIL']
]);

$seeder->refill();