<?php


namespace test\database;

// singleton class
/**
 * implemented as a singleton 
 * @class connection
 * @author Strahinja Mihajlovic
 */
class connection
{
    /**
     * @var connection $object
     */
    private static $object;
    /**
     * parameters for database login
     * @var string 
     *
     */ 
    private  $host, $user, $pass;
    /**
     * singleton parameter for a class
     * @var \PDO
     */
    private $connection;
    
    /**
     * returns connection to database
     * @return \PDO|null
     */
    public static function getConnection() : ?\PDO{
        if(!isset($object)){
            self::$object = new connection();
        }
        return self::$object->connection;
    }
    /**
     * private constructor
     * @return void
     */
    private function __construct()
    {
        $this->user = $_ENV['DB_USER'];
        $this->pass = $_ENV['DB_PASS'];
        $this->host= "mysql:dbname=". $_ENV["DB_NAME"] .";host=". $_ENV['DB_HOST'];
        $this->connection = new \PDO($this->host, $this->user, $this->pass);
    }


}