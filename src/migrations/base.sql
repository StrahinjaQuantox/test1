-- Put here your base SQL
create database if not exists test;
use test;

drop table if exists users;
drop table if exists proba;


create table users(
	id int not null auto_increment,
	username varchar(20) not null,
	email varchar(40) not null unique,
	pass varchar(80) not null,
	primary key(id)
);
