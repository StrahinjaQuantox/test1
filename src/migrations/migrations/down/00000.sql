-- Migrate to Version 0 
alter table users
drop foreign key fk_role_user,
drop index fk_role_user,
drop column role_id;


drop table if exists perm_role ;
drop table if exists roles;
drop table if exists permissions;
