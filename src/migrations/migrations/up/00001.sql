-- Migrate to Version 1 
use test;
create table roles(
    id int auto_increment primary key,
    name varchar(20) not null
);

create table permissions(
     id int auto_increment primary key,
     name varchar(20) not null
);

create table perm_role(
    id_role int not null,
    id_perm int not null,
    constraint fk_role foreign key(id_role)
    references roles(id)
    on update cascade
    on delete cascade,
    constraint fk_perm foreign key(id_perm)
    references permissions(id)
    on update cascade
    on delete cascade,
     constraint perm_role_u unique(id_perm, id_role)
);

alter table users
add column role_id int;

alter table users
add constraint fk_role_user
foreign key(role_id)
references roles(id);
