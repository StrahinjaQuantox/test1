<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace test\models;

/**
 * Extends User with permission controls
 *
 * @author quantox
 */
class PrivilegedUser extends User {
    /**
     *
     * @var test\models\Role|array<string>
     */
    private $role;
    
    /**
     * 
     * @param string $username
     */
    public function setUser($username){
        parent::setUser($username);
        $this->initRoles();
    }
    
    /**
     * 
     * @param string $role
     * @return bool
     */
    public function setRole($role){
        $roleQuery = \test\database\connection::getConnection()
                ->prepare('select name, id as role_id from roles where name = ?');
        $roleQuery->execute([$role]);
        $roleFetched = $roleQuery->execute([$role])->fetch(\PDO::FETCH_ASSOC);
        $setUserRole = \test\database\connection::getConnection()
                ->prepare("update users set role_id = ? where id = $this->id");
        
        return $setUserRole->execute([$roleFetched['id']]);
    }
    
    /**
     * Overrides function from User class
     * @return bool
     * @throws InputException
     */
    protected function validateUserBeforeInsert() {
        $this->role_id = (int)Role::getRoleByName('restricted')['id'];
        return parent::validateUserBeforeInsert();
    }
    
    /**
     * Initialising role
     */
    private function initRoles(){
        $QueryString = "SELECT t1.role_id, t2.name FROM users as t1
                JOIN roles as t2 ON t1.role_id = t2.id
                WHERE t1.id = ?";
        $statement = \test\database\connection::getConnection()->prepare($QueryString);
        $statement->execute([$this->id]);
        $this->role = $statement->fetch(\PDO::FETCH_ASSOC);
    }
    
    /**
     * checks if current user has privileges
     * @param string $isPriv
     * @return bool
     */
    public function hasPrivilege($isPriv){
        $this->role = Role::getRolePerms($this->role['role_id']);
        return $this->role->hasPerm($isPriv);
    }
}
