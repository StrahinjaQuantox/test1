<?php
namespace test\models;
/**
 * Responsible for role management
 * @class Role
 * @author Strahinja Mihajlovic
 */

class Role
{
    /**
     *
     * @var array<bool>
     */
    protected $permissions;

    protected function __construct() {
        $this->permissions = array();
    }

    
    /**
     * // return a role object with associated permissions
     * @param int $role_id
     * @return \test\models\Role
     */
    public static function getRolePerms($role_id) {
        $role = new Role();
        $sqlQuery = "SELECT t2.name FROM perm_role as t1
                JOIN permissions as t2 ON t1.id_perm = t2.id
                WHERE t1.id_role = ?";
        $statement = \test\database\connection::getConnection()->prepare($sqlQuery);
        $statement->execute([$role_id]);
        
        foreach($statement->fetchAll(\PDO::FETCH_ASSOC) as $value){
            $role->permissions[$value["name"]] = true;
        }
        return $role;
    }

    /**
     * returns role with searched name
     * @param string $role
     * @return array<string>
     */
    public static function getRoleByName($role){
        $sqlQuery = "select * from roles where name = ?";
        $statement = \test\database\connection::getConnection()->prepare($sqlQuery);
        $statement->execute([$role]);
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }
    
   
    /**
     * check if a permission is set
     * @param string $permission
     * @return bool
     */
    public function hasPerm($permission) {
        return isset($this->permissions[$permission]);
    }
}