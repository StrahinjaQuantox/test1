<?php

/**
 * Abstract class containing shared model functions
 * @class model
 * @author Strahinja Mihajlovic
 */
namespace test\models;
use test\database\connection;

abstract class model
{
    public function getConnection() : ?\PDO{
        return connection::getConnection();
    }

}