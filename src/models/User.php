<?php


namespace test\models;
use test\errors\InputException;

/**
 * Model for managing users
 * @class User
 * @author Strahinja Mihajlovic
 */

class User extends model
{
    /**
     *
     * @var string
     */
    
    
    protected $role_id,  $pass;
    /**
     *
     * @var string
     */
    public $id, $username, $email;

    /**
     * fills in the user data from the database
     * @global array<string> $data
     * @param string $username
     * @return boolean|array<string>
     */
    public function setUser($username){
       if(filter_var( $username, FILTER_VALIDATE_EMAIL) !== false){
            $data = $this->QueryUser(['email' => $username]);
        }else{
           $data =  $this->QueryUser(['username' => $username]);
        }
        if(empty($data)){
            return false;
        }
        $this->username = $data["username"];
        $this->email = $data["email"];
        $this->pass = $data['pass'];
        $this->id = $data['id'];
        return true;
    }
    /**
     * returns user by id
     * @param int $userId
     * @return array<string>
     */
    public function getUserById($userId){
        return $this->QueryUser(['id' => $userId]);
    }
    
    /**
     * return one specific user
     * @global \PDOStatement $query
     * @param array<string> $array
     * @return array<string>
     */
    protected function QueryUser($array)
    {
        $iteration = 0;
        $varArray = [];
        $stringQuery = "select id, username, email, pass from users where ";
        foreach($array as $key => $item){
            if(!$iteration++){
            $stringQuery .= " $key = ?";
            }else{
                $stringQuery .= " AND $key = ?";
            }
            array_push($varArray, $item);
            
        }
        $query = $this->getConnection()
                ->prepare($stringQuery);
        
        $query->execute($varArray);
        
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * fills in the user data
     * @param array<string> $array
     * @return \test\models\Exception|void
     */
    /**
     * Procedure for filling in the user data
     * @param array<string> $array
     */
    public function fillInUserData($array){
        
        if(isset($array['username'])){
            $this->username = $array['username'];
        }
        if(isset($array['password'])){
            $this->pass = $array['password'];
        }
        if(isset($array['email'])){
            $this->email = $array['email'];
        }
        if(isset($array['role_id'])){
            $this->role_id = $array['role_id'];
        }
        if(isset($array['id'])){
            $this->id = $array['id'];
        }
    }
    
    /**
     * 
     * @param array<string> $array
     * @return bool|\test\models\Exception
     */
    public function createUser($array){
        $this->fillInUserData($array);
        
        try{
            return $this->validateUserBeforeInsert();
        } catch (Exception $e){
            return $e;
        }

    }
    /**
     * 
     * @param mixed $array
     * @param int|string $userId
     * @return bool
     */
    public function updateUser($array, $userId){
        try{
        $this->fillInUserData($array);
        $this->validateUserDataBeforeUpdate($userId);
        } catch (Exception $e){
            return $e;
        }
        $query = $this->getConnection();
        $stringQuery = 'update users set ';
        $varArray = [];
        $iteration = 0;
        foreach (get_object_vars($this) as $var => $value){
            if(empty($value)){
                continue;
            }elseif($var === "password"){
                 $this->pass = password_hash($this->pass,PASSWORD_BCRYPT);
                 $stringQuery .= $iteration++ ? ',':'';
                $stringQuery .= "pass = ? ";
                array_push($varArray, $this->pass);
                continue;
            }
                $stringQuery .= $iteration++ ? ',':'';
                $stringQuery .= "$var = ? ";
                array_push($varArray, $value);
            
        }
        $stringQuery .= "where id = $userId";
        $statement = $query->prepare($stringQuery);
        
        return $statement->execute($varArray);
    }
    /**
     * 
     * @return string
     */
    public function deleteMe(){
         $useId = $_GET['userId'];   
        if($this->QueryUser(["id" => $this->id])){
            $statement = $this->getConnection()->prepare("delete from users where id = ?");
            return $statement->execute([$this->id]) ? 'success! <a href="/">go back</a>'
                    :'fail! <a href="/">go back</a>';
        }
        return 'fail! <a href="/">go back</a>';
    }
    
/**
 * 
 * @param int|string $userId
 * @throws InputException
 */
    private function validateUserDataBeforeUpdate($userId){
        if($this->QueryUser(['id' => $userId]) == false){
            throw new InputException('User not found', 4);
        }elseif(empty($this->email) && empty($this->username) && empty($this->pass)){
            throw new InputException('Nothing to change here', 5);
        }elseif(!filter_var($this->email, FILTER_VALIDATE_EMAIL) && !empty ($this->email)){
            throw new InputException("Invalid e-mail adress", 2);
        }
    }
    
    /**
     * prepares the database connection and executing it
     * @return bool
     * @throws InputException
     */
    protected function validateUserBeforeInsert(){
        $this->validateUserDataForCreate();
        $this->pass = password_hash($this->pass,PASSWORD_BCRYPT);
       
        
        /* @var $query \PDO*/
        /* @var $statement \PDOStatement*/
        $query = $this->getConnection();
        $columns = get_object_vars($this);
        unset($columns['id']);
        $queryString = "insert into users(" . implode(', ',array_keys($columns)) . ") ";
        $queryString .= "values( " . str_repeat(" ? ,", count($columns) - 1) . ' ? )';
        $statement = $query->prepare($queryString);
       
        return $statement->execute(array_values($columns));
       
         
    }

    private function validateUserDataForCreate(){
        if (empty($this->username) || empty($this->email) || empty($this->pass)){
            throw new InputException("empty inputs", 0); //vracamo exception ako neka polja nisu popunjena
        }elseif($this->QueryUser(['username' => $this->username]) != false
            || $this->QueryUser(['email' => $this->email]) != false){
             throw new InputException("user already exists", 1); //vracamo exception ako postoji korisnik
        }elseif(!filter_var($this->email, FILTER_VALIDATE_EMAIL)){
            throw new InputException("Invalid e-mail adress", 2);
        }
    }
    
    /**
     * validate the password
     * @param string $password
     * @return bool
     */
    public function validateLogin($password){
        
        return password_verify($password, $this->pass);
    }
    
    /**
     * 
     * @return array<array<string>>
     */
    public static function returnAllUsers(){
        $query = \test\database\connection::getConnection()->query("select id, username, email from users");
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }
}