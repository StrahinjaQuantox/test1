<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace test\errors;

/**
 * Used for standard user data validation errors
 *
 * @author Strahinja Mihajlovic
 */
class InputException extends \Exception{
    public function construct($message, $code){
        parent::__construct($message, $code);
    }
}
