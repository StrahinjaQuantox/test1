<?php
namespace test\controllers;
use test\models\User;


/**
 * A controller for home
 * @class IndexController
 * @author Strahinja Mihajlovic
 */
class IndexController extends controller
{
    /**
     * shows a home page to the user
     * @global array<array<string>> $userArray
     * @return string
     */
    
    public function index()
    {
        $userArray = [];
        $this->view = new \test\views\IndexView();
        if(isset($_SESSION['isLogged'])){
            $userArray = User::returnAllUsers();
        }
        
        return $this->view->render($userArray);

    }
}