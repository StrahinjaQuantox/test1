<?php
namespace test\controllers;
/**
 * Abstract class for controllers
 * @class controller
 * @author Strahinja Mihajlovic
 */
abstract class controller{
    /**
     *
     * @var  model
     */
    protected $model;
    /**
     *
     * @var view
     */
    protected $view;
    /**
     * @return string
     */
    
    public abstract function index();
}
