<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace test\controllers;

use test\views\LoginView;
/**
 * Description of AuthController
 *
 * @author quantox
 */
class AuthController extends controller {
  
    public function index(): string {
        
       
        if(isset($_SESSION['isLogged'])){
            header('location: /');
        }

    
    }
    
    /**
     * function for registration procedure
     * @return string
     */
    public function registration($type = 'register here')
    {
        $data = [];
        $data['username'] = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $data['email'] = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $data['password'] = filter_input(INPUT_POST, 'password');
        $data['role_name'] = 'restricted';
        $this->view = new \test\views\RegistrationView();
        if(!empty($data) && $data != false){
           /* @var $user test\models\User */
            
           $user = new \test\models\PrivilegedUser();
           try{
           return $user->createUser($data) 
                   ? 'you have registered successfully' . $this->view->render($type)
                   :  $this->view->render($type);
           } catch (\test\errors\InputException $e){
               return $e->getMessage() . $this->view->render($type);
           }
       }
       
       return $this->view->render($type);
       
       
    }

    /**
     * function for login
     * @return string
     */
    public function login(){
        $data = [];
        $data['user'] = filter_input(INPUT_POST, 'user'
            , FILTER_SANITIZE_STRING);
        $data['password'] = filter_input(INPUT_POST, 'password');

        if(!empty($data)){
            $this->model = new \test\models\User();
            $this->model->setUser($data['user']);
            
            if($this->model->validateLogin($data['password'])){
                session_start();
                $_SESSION['user'] = $this->model->username;
                $_SESSION["isLogged"] = true;
                $this->index();
            }
        }
        $this->view = new LoginView();
        return $this->view->render([]);
    }
    /**
     * function for logout
     * @return void
     */
    public function logout(){
        session_start();
        session_destroy();
        session_unset();
        session_abort();
        header("location:/");
    }
    
    
    
}
