<?php
namespace test\controllers;

use test\views\LoginView;
use test\models\User;
/**
 * Responsible for the user management
 * @class UserController
 * @author Strahinja Mihajlovic
 */

class UserController extends controller{

    /**
     * if successful login, redirect user to home
     * @return void
     */
    public function index(){
       
        if(isset($_SESSION['isLogged'])){
            header('location: /');
        }

    }
    
    
    
    /**
     * return view of the one user
     * @param int $userId
     * @return string
     */
    public function view($userId){
        $user = filter_var($userId,FILTER_SANITIZE_URL);
        $this->model = new \test\models\User();
        $this->view = new \test\views\ViewUser();
        $this->model->setUser($user);
        return $this->view->render($this->model);
    }
    
    /**
     * checks user privileges and then registers user
     * @return string
     */
    public function create(){
        if($this->checkPrivilege('user_management')){
        
            return $this->checkPrivilege('user_management');
        }
        
       return $this->registration('create a new user');
    }
    
    /**
     * 
     * @param int $user
     * @return string
     */
    public function update($user){
       
        if($this->checkPrivilege('user_management')){
        
            return $this->checkPrivilege('user_management');
        }
        
        if(!isset($user)){
            return 'No user to edit, <a href="/">go back</a>';
        }
        
        $userId = filter_var($user, FILTER_SANITIZE_NUMBER_INT);
        $this->model = new \test\models\User();
        $this->view = new \test\views\UpdateView();
        
        if(filter_input(INPUT_SERVER, "REQUEST_METHOD") === "POST"){
            $data = [];
            $data['username'] = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $data['email'] = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            $data['password'] = filter_input(INPUT_POST, 'password');
            try{
            return $this->model->updateUser($data, $userId)
                   ? 'you have updated user successfully' . $this->view->render($this->model->getUserById($userId))
                   :  $this->view->render($this->model->getUserById($userId));
            } catch (\test\errors\InputException $e){
                   return $e->getMessage() . $this->view->render($this->model->getUserById($userId));
            }
        }
        $this->view->render($this->model->getUserById($userId));
    }
    
    /**
     * 
     * @param type $privilege
     * @return boolean|string
     */
    private function checkPrivilege($privilege){
        
        if(!isset($_SESSION['user'])){
            return 'Cant view, you need to log in <a href="/">Go back </a>';
        }
        
        $loggedUser = new \test\models\PrivilegedUser();
        $loggedUser->setUser($_SESSION['user']);
        if(!$loggedUser->hasPrivilege($privilege)){
            return 'You don\'t have necessary permissions for this action!'
            . '<a href= "/">Go back</a>';
        }
        return false;
    }
    
    
    /**
     * checks privileges first and then deletes user
     * @return string
     */
    public function delete(){
        
         if($this->checkPrivilege('user_management')){
        
            return $this->checkPrivilege('user_management');
        }
        $this->model = new \test\models\User();
        if(!isset($_GET['userId']) && !filter_input(INPUT_GET, 'userId', FILTER_VALIDATE_INT)){
            return 'invalid user identification, must be a number!';
        }
        $userId = filter_input(INPUT_GET, 'userId', FILTER_SANITIZE_NUMBER_INT);
        $this->model->fillInUserData($this->model->getUserById($userId));
        
        return $this->model->deleteMe();
        
    }
}