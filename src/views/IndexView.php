<?php


namespace test\views;

/**
 * Home view
 * @class IndexView
 * @author Strahinja Mihajlovic
 */
class IndexView implements view
{

    /**
     * includes and returns html for the home page
     * @param array<string> $array
     * @return string
     */
    public function render($array)
    {
       include "html/index.php";
    }
}