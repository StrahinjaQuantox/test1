<?php

/**
 * @global array $array 
 */
?>

<h1>Hello world! <?php echo isset($_SESSION['isLogged']) ? "And hello " 
        . filter_var($_SESSION['user'], FILTER_SANITIZE_STRING): '';?></h1>
<?php if(isset($_SESSION['isLogged'])) : ?>
<a href="logout">Logout</a>
<?php else : ?>
<a href="login">Login</a>
<a href="registration">Registration</a>
<?php endif; ?>
<?php 
    if(isset($_SESSION['isLogged'])) :
?>
    <table>
        <tr>
            <th>Username</th>
            <th>Email</th>
            <th>Commands</th>
            <th>
                <a href='create'><button>
                    create a new user
                </button>
                </a>
            </th>
        </tr>            
        <?php 
        foreach ($array as $user):
        ?> 
        <tr>
            <td> <?= $user['username'] ?></td>
            <td> <?= $user['email'] ?> </td>
            <td>
                <a href='/view/<?= $user['username'] ?>'>
                 <button>
                    view
                </button>
                </a>
                <a href='update/<?= $user['id'] ?>'>
                 <button>
                    update
                </button>
                </a>
                 <a href='delete?userId=<?= $user['id'] ?>'>
                 <button>
                    delete
                </button>
                </a>
            </td>
        <?php
        endforeach;
        ?>
    </table>


    <?php endif; ?>
