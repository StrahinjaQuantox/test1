<?php


namespace test\views;

/**
 * View for the login page
 * @class LoginView
 * @author Strahinja Mihajlovic
 */
class LoginView implements view
{

    /**
     * returns rendered page for the login
     * @param array<string> $array
     * @return string
     */
    public function render($array)
    {
        include "html/login.php";
    }
}