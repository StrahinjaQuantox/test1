<?php

/**
 * Interface containing shared view functions
 * @class view
 * @author Strahinja Mihajlovic
 */
namespace test\views;


interface view
{
    
    /**
     * Rendering specific pages
     * @param array<string> $array
     * 
     * @return string
     */
    public function render($array);
}