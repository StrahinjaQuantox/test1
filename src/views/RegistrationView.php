<?php


namespace test\views;

/**
 * View for the registration page
 * @class RegistrationView
 * @author Strahinja Mihajlovic
 */
class RegistrationView implements view
{

    /**
     * returns rendered page for the registration
     * @param mixed $array
     * @return string
     */
    public function render($array)
    {
        
            include_once 'html/registration.php';
    }
}